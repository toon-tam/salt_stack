const { Salt } = require("salt-api");
const pg = require("pg");
const saltdb_v2 = require("./saltdb_v2.js")
const dotenv = require('dotenv');
const { Pool, Client } = pg
dotenv.config()
const salt = new Salt({
	url: process.env.SALT_URL,
	username: process.env.SALT_USER,
	password: process.env.SALT_PASSWORD,
	eauth: process.env.SALT_EAUTH
});

const client = new Client({
	user: process.env.POSTGRES_USER,
	password: process.env.POSTGRES_PASSWORD,
	host: process.env.POSTGRES_ENDPOINT,
	port: process.env.POSTGRES_PORT,
	database: process.env.POSTGRES_DB,
});
const saltDB = new saltdb_v2(salt, client);

client
	.connect()
	.then(async () => {
			await saltDB.get_event()
	})
	.catch((err) => {
		console.error('Error connecting to PostgreSQL database', err);
	});