
// const request = require('request');
// const dotenv = require('dotenv');
const EventEmitter = require('node:events');
const salt_stack_v2 = require('./database/salt_stack_v2.js');

class saltdb_v2 {
    constructor(salt, client) {
        this.salt = salt;
        this.client = client;
        this.salt_stack = new salt_stack_v2(this.client);
        this.emitter = new EventEmitter();
        this.on = this.emitter.on;
        this.removeAllListeners = this.emitter.removeAllListeners;
        this.emit = this.emitter.emit;
        this.minions = []
    }
    async get_date() {
        let d = new Date();
        let year = d.getFullYear();
        let month = d.getMonth() + 1;
        let day = d.getDate()
        let hours = d.getHours() + 7
        let min = d.getMinutes();
        let sec = d.getSeconds()
        let time = year + "-" + month + "-" + day + " " + hours + ":" + min + ":" + sec
        // console.log(time);
        return time;
    }

    // async line_notify(minion_id) {
    //     for (let index = 0; index < 5; index++) {
    //         if (index == 4) {
    //             dotenv.config();
    //             const url_line_notification = "https://notify-api.line.me/api/notify";
    //             request({
    //                 method: 'POST',
    //                 uri: url_line_notification,
    //                 header: {
    //                     'Content-Type': 'multipart/form-data',
    //                 },
    //                 auth: {
    //                     bearer: process.env.TOKEN,
    //                 },
    //                 form: {
    //                     message: minion_id + ' is Offline'
    //                 },
    //             }, (err, httpResponse, body) => {
    //                 // console.log(httpResponse);
    //                 if (err) {
    //                     console.log(err)
    //                 } else {
    //                     console.log(body)
    //                 }
    //             });
    //         }
    //     }
    // }

    async update_status_user(uuid, status, time) {
        try {
            const response = await new Promise((resolve, reject) => {
                    this.salt_stack.update_status_user(uuid, status, time).then(data => {
                        // console.log('update success');
                        resolve(data);
                    }).catch( (err) => {
                        // console.log('update status user');
                        reject(err);
                    })
            })
            return {statusCode: 200}

        } catch (error) {
            return {statusCode: 400}
        }


    }

    async insert_userinfo(uuid,minion_name, status, time) {
        try {
            const response = await new Promise((resolve, reject) => {
                    this.salt_stack.insert_userinfo_db(uuid, minion_name, status, time, time).then(data => {
                        resolve(data);
                    }).catch( (err) => {
                        // console.log('error insert new status user');
                        // console.log(err);
                        reject(err);
                    })
            })
            return {statusCode: 200}

        } catch (error) {
            return {statusCode: 400}
        }
    }

    async update_spec_cpu(user_id, sum_total_cpu, time){
        try {
            const response = await new Promise((resolve, reject) => {
                    this.salt_stack.update_cpu_user(user_id, 'Online', sum_total_cpu, time).then(data => {
                        resolve(data);
                    }).catch( (err) => {
                        reject(err);
                    })
            })
            return {statusCode: 200}

        } catch (error) {
            return {statusCode: 400}
        } 
    }

    async update_spec_mem(user_id,MemTotal,time){
        try {
            const response = await new Promise((resolve, reject) => {
                this.salt_stack.update_mem_user(user_id, 'Online', MemTotal, time).then(data => {
                    // console.log("update mem user");
                        resolve(data);
                    }).catch( (err) => {
                        // console.log('error spec mem');
                        reject(err);
                    })
            })
            return {statusCode: 200}

        } catch (error) {
            return {statusCode: 400}
        } 
    }

    async insert_cpu(user_id, persent_cpu_used,vcpu, time) {
        try {
            const response = await new Promise((resolve, reject) => {
                    this.salt_stack.insert_logs_cpu(user_id, persent_cpu_used,vcpu, time).then(data => {
                        resolve(data);
                    }).catch( (err) => {
                        // console.log(err);
                        reject(err);
                    })
            })
            return {statusCode: 200}

        } catch (error) {
            return {statusCode: 400}
        }
    }

    async insert_mem(uuid, percent_mem,Cached,Buffers,percent_mem_ava, time) {
        try {
            const response = await new Promise((resolve, reject) => {
                    this.salt_stack.insert_logs_mem(uuid, percent_mem,Cached,Buffers,percent_mem_ava, time).then(data => {
                        resolve(data);
                    }).catch( (err) => {
                        // console.log(err);
                        // console.log('error logs memory');
                        reject(err);
                    })
            })
            return {statusCode: 200}

        } catch (error) {
            return {statusCode: 400}
        }
    }

    async insert_load(user_id, one_min, five_min, fifteen_min, time) {
        try {
            const response = await new Promise((resolve, reject) => {
                this.salt_stack.insert_logs_load(user_id, one_min, five_min, fifteen_min, time).then(data => {
                        resolve(data);
                    }).catch( (err) => {
                        // console.log('error logs load');
                        reject(err);
                    })
            })
            return {statusCode: 200}

        } catch (error) {
            return {statusCode: 400}
        }
    }

    async check_status_minion(minion, time) {
        var all_minion = await this.get_minion()
        if (this.minions.length == 0) {
            this.minions.push(minion)
        } else if (this.minions.length > 0) {
            var s_minion = this.minions.includes(minion)
            if (s_minion == false) {
                this.minions.push(minion)
            } else if (s_minion == true) {
                for (let index = 0; index < all_minion.length; index++) {
                    const element = all_minion[index];
                    const uuid = element.uuid
                    const status = element.status
                    const off_minion = this.minions.includes(uuid)
                    const updated_time = element.updated_time
                    const created_time = element.created_time
                    // console.log(updated_time);
                    const isoDate = created_time
                    const date = new Date(isoDate);
                    const year = date.getFullYear();
                    const month = String(date.getMonth() + 1).padStart(2, '0');
                    const day = String(date.getDate()).padStart(2, '0');
                    const hours = String(date.getHours()).padStart(2, '0');
                    const minutes = String(date.getMinutes()).padStart(2, '0');
                    const seconds = String(date.getSeconds()).padStart(2, '0');
                    const formattedDate = `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
                    console.log(off_minion);
                    if (off_minion == false) {
                        console.log(uuid);
                        this.update_status_user(uuid, 'Offline', formattedDate)
                    }
                    
                }
            }
        }
        console.log(this.minions);
    }



    async get_minion() {
        var get_status_minion = await this.salt_stack.get_user()
        var all_minion = get_status_minion.rows
        return all_minion
    }

    async get_event() {
        //////////////////// event \\\\\\\\\\\\\\\\\\\\\\\\
        this.salt.login().then(async () => {
            const events = await this.salt.eventSource()
            events.onopen = () => {
                console.log("Connected to Salt Master Event Bus")
            }
            events.onmessage = async (data) => {
                // console.log("Got event from Salt Master Events Bus", data)
                data = JSON.parse(data.data).data
                // console.log(JSON.stringify(data));
                console.log(data);

                if (data.data != undefined && data.data != 'ping' && data.data.cpustats != undefined) {
                    // console.log("data===>",data);
                    var all = data.data
                    var cpustats = all.cpustats
                    var meminfo = all.meminfo
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1].split('.')[0]
                    var user_id = data.id
                    var loadavg = all.loadavg
                    // console.log(all);
                    let kB = 9.5367
                    if (cpustats.cpu != undefined) {
                        var cpu = cpustats.cpu
                        // console.log("cpu====>", cpu);
                        const idle = cpu.idle
                        const softirq = cpu.softirq
                        const iowait = cpu.iowait
                        const steal = cpu.steal
                        const system_use = cpu.system
                        const user_use = cpu.user
                        const total_cpu = idle + system_use + user_use
                        const cpu_used = 1 - (idle / total_cpu)
                        const persent_cpu_used = cpu_used * 100
                        // console.log(persent_cpu_used);
                        const sum_total_cpu = total_cpu + softirq + iowait + steal
                        const gb_total_cpu = (sum_total_cpu * kB) / 10000000
                        var vcpu = []
                        for (let index = 0; index < 5; index++) {
                            const e = "cpu" + index;
                            // console.log(cpustats[element]);
                            if (cpustats[e] != undefined) {
                                // console.log(e);
                                const core = e
                                // console.log(core);
                                const value = cpustats[e]
                                
                                vcpu.push([core,value])

                            }
                        } 
                        console.log(vcpu);    
                        let obj = Object.fromEntries(vcpu); 
                        let obj_vcpu = JSON.stringify(obj)    
                        this.insert_cpu(user_id, persent_cpu_used,obj_vcpu, time)
                        this.update_spec_cpu(user_id,gb_total_cpu,time)
                    }
                    if (meminfo.MemTotal != undefined) {
                        // console.log(meminfo);
                        let MemTotal = parseFloat(meminfo.MemTotal.value)
                        let MemFree = parseFloat(meminfo.MemFree.value)
                        let Cached = parseFloat(meminfo.Cached.value)
                        let Buffers = parseFloat(meminfo.Buffers.value)
                        let MemAvailable = parseFloat(meminfo.MemAvailable.value)
                        // console.log("MemTotal===>",MemTotal);
                        // console.log("MemAvailable==>",MemAvailable);
                        let MemUse = MemTotal - (MemFree + Cached + Buffers)
                        let percent_mem = (MemUse / MemTotal) * 100
                        let percent_mem_ava = (MemAvailable / MemTotal) * 100
                        let gb_total_mem = (MemTotal * kB) / 10000000
                        // console.log("percent_mem_ava====>", percent_mem_ava);
                        // console.log("percent_mem====>", percent_mem);
                        this.insert_mem(user_id, percent_mem,Cached,Buffers,percent_mem_ava, time)
                        this.update_spec_mem(user_id,gb_total_mem,time)
                        
                    }
                    if (loadavg) {
                        var load_avg = Object.entries(loadavg)
                        let one_min = load_avg[0][1]
                        let five_min = load_avg[1][1]
                        let fifteen_min = load_avg[2][1]
                        // console.log(one_min);
                        // console.log(five_min);
                        // console.log(fifteen_min);
                        this.insert_load(user_id, one_min, five_min, fifteen_min, time)
                    }
                    this.check_status_minion(user_id, time)
                } else if (data.data == 'ping') {
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                    var uuid = data.id
                    this.insert_userinfo(uuid,uuid, 'Online', time)

                } else if (data.tag == 'minion_start') {
                    // console.log('data.tag');
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                    this.insert_userinfo(data.id,data.id, 'Online', time)

                } else if (data.pub) {
                    console.log('Salt minion will wait for 10 seconds before attempting to re-authenticate');
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                    this.insert_userinfo(data.id,data.id, 'NotConnect', time)

                } else if (data.minions) {
                    console.log('minion===>', data.minions);
                    for (let index = 0; index < data.minions.length; index++) {
                        const element = data.minions[index];
                        // console.log(element);
                        const time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                        this.insert_userinfo(element,element, 'Online', time)

                    }
                } else if (data.cmd == '_minion_event') {
                    var time = data._stamp.split('T')[0] + " " + data._stamp.split('T')[1]
                    this.insert_userinfo(data.id,data.id, 'Online', time)
                } else {
                    console.log("Got event from Salt Master Events Bus", data)
                    return data
                }

            }

            events.onerror = async (err) => {
                console.log("Got event from Salt Master Events err", err)
                if (err?.status === 401 || err?.status === 403) {
                    console.log("Not authorized")
                    this.get_event()
                };
                return err
            }

        })
        //////////////////// event \\\\\\\\\\\\\\\\\\\\\\\\
    }

}
module.exports = saltdb_v2

