# ใช้ base image เป็น Ubuntu
FROM ubuntu:20.04

# ตั้งค่า environment variables เพื่อไม่ให้ Docker รอการป้อนข้อมูลจากผู้ใช้
ENV DEBIAN_FRONTEND=noninteractive

# อัพเดต package list และติดตั้ง dependencies
RUN apt-get update && apt-get install -y \
    curl \
    gnupg2 \
    lsb-release \
    && rm -rf /var/lib/apt/lists/*

# เพิ่ม repository ของ SaltStack
RUN curl -fsSL https://repo.saltproject.io/py3/ubuntu/20.04/amd64/latest/SALTSTACK-GPG-KEY.pub | apt-key add -
RUN echo "deb http://repo.saltproject.io/py3/ubuntu/20.04/amd64/latest focal main" > /etc/apt/sources.list.d/saltstack.list

# อัพเดต package list และติดตั้ง Salt Master และ Salt API
RUN apt-get update && apt-get install -y \
    salt-master \
    salt-api \
    && rm -rf /var/lib/apt/lists/*
RUN useradd -m -s /bin/bash saltuser && echo "saltuser:saltpassword" | chpasswd
# เปิดพอร์ตที่ใช้โดย Salt Master และ Salt API
EXPOSE 4505 4506 8000

# คัดลอกไฟล์การตั้งค่า (ถ้ามี) ไปยังตำแหน่งที่ถูกต้อง
COPY ./master /etc/salt/master
COPY ./salt-api.service /etc/systemd/system
COPY ./salt-master.service /etc/systemd/system

# เริ่มต้น Salt Master และ Salt API เมื่อ container รัน
CMD ["sh", "-c", "salt-master -d && salt-api -d && tail -f /var/log/salt/master"]
